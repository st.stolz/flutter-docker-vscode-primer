FROM ubuntu:18.04
# Prerequisites
RUN apt update && apt install -y curl git unzip xz-utils zip libglu1-mesa openjdk-8-jdk wget
# Setup new user
RUN useradd -ms /bin/bash developer
# RUN usermod -a -G plugdev developer
#USER developer
WORKDIR /home/developer
# Prepare Android directories and system variables
RUN mkdir -p Android/Sdk
ENV ANDROID_SDK_ROOT /home/developer/Android/Sdk
RUN mkdir -p .android && touch .android/repositories.cfg
# Setup Android SDK
RUN wget -O sdk-tools.zip https://dl.google.com/android/repository/commandlinetools-linux-6609375_latest.zip
RUN unzip sdk-tools.zip && rm sdk-tools.zip
RUN mkdir Android/Sdk/cmdline-tools
RUN mv tools Android/Sdk/cmdline-tools
ENV PATH "$PATH:/home/developer/Android/Sdk/cmdline-tools/tools/bin"
RUN cd Android/Sdk/cmdline-tools/tools/bin && yes | sdkmanager --licenses
RUN cd Android/Sdk/cmdline-tools/tools/bin && sdkmanager "build-tools;29.0.2" "patcher;v4" "platform-tools" "platforms;android-29" "sources;android-29"
# Download Flutter SDK
RUN git clone https://github.com/flutter/flutter.git
RUN cd flutter && git checkout tags/2.2.0 
ENV PATH "$PATH:/home/developer/flutter/bin"
# Run basic check to download Dark SDK
RUN flutter config --enable-web
# RUN flutter upgrade
RUN flutter pub global activate webdev