# Flutter Docker Container

Flutter Workspace in Docker Container (adapted for Android 11 from https://medium.com/@cezary.zelisko/how-to-prepare-a-flutter-workspace-in-a-docker-container-e56e0c7c7dcd)

## How To use

1. Install "Remote - Containers" VSCode Extension
2. Open folder containing Dockerfile in VSCode
3. Execute `Remote Containers: Open folder in container ...` in VSCode
4. Select again folder containing Dockerfile
5. Create new App with `flutter create <app_name>` via Command Line
6. Select "File" > "Open Folder ..." and open folder containing app

## How to start Android Emulator

1. You can not use the Emulator in Windows, because WSL2 does not support nested virtualization yet
1. Check if quemu is installed and running on the host ([Read how to do this] (https://www.tecmint.com/install-kvm-on-ubuntu/)
2. Open the Remote Container and execute `flutter emulators --launch flutter_emulator`
3. Open http://ip-to-system-running-docker:8080/vnc.html on host
4. Run `flutter run`

## How to create project from scratch

1. Go to /workspace in Container Terminal
1. Run `flutter create <app_name>`

## How to start web preview

1. Go to /workspace/<app_name> in Container Terminal
2. To run flutter web, execute `flutter run -d web-server --web-port 8998`


